(ns plf01.core
  (:gen-class))

(defn funcion-<-1
  [n1, n2] 
  (< n1 n2))

(defn funcion-<-2
  [n1, n2]
  (< (* 2 n1) n2))
  

(defn funcion-<-3
  [n1, n2, n3]
  (< n1 n2 n3))



(funcion-<-1 2 3)
(funcion-<-2 3 4)
(funcion-<-3 5 6 7)

(defn funcion-<=-1
  [n1, n2]
  (<= n1 n2))

(defn funcion-<=-2
  [n1, n2]
  (<= n1 (* 2 n2)))

(defn funcion-<=-3
  [n1, n2, n3, n4]
  (<= n1 n2 n3 (+ n3 n4)))


(funcion-<=-1 2 3)
(funcion-<=-2 3 4)
(funcion-<=-3 5 6 7 9)

(defn funcion-==-1
  [n1, n2]
  (== n1 n2))

(defn funcion-==-2
  [n1, n2, n3]
  (== (+ n1 n2) n3))

(defn funcion-==-3
  [n1, n2, n3, n4]
  (== (+ n1 n2) (+ n3 n4)))


(funcion-==-1 2 3)
(funcion-==-2 3 4 5)
(funcion-==-3 5 6 7 8)

(defn funcion-assoc-1
  [n1, n2, n3, n4]
  (assoc [n1 n2 n3 n4] 2 8))


(defn funcion-assoc-2
  [n1, n2, n3, n4]
  (assoc [n1, n2, n3, (+ n4 n1)] 0 10))

(defn funcion-assoc-3
  [n1 n2 n3]
  (assoc [n1 n2 n3] 3 9))


(funcion-assoc-1 2 3 5 6)
(funcion-assoc-2 3 4 8 9)
(funcion-assoc-3 5 6 7)



